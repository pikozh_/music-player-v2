package utils;

import com.devEducation.model.Song;
import com.google.gson.Gson;

import java.util.List;

public class JSONSender {
    public static String sendJSONResponse(List<Song> songList) {
        Gson gson = new Gson();
        String response = gson.toJson(songList);
        return response;
    }
}
