package com.devEducation.repository;

import com.devEducation.model.Song;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLRepository {
    private static final String URL = "jdbc:mysql://localhost:3306/player?useUnicode=true&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "Lokolol12345";
    private Statement statement;
    private ResultSet resultSet;
    private Connection connection;


    public SQLRepository() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            if (connection != null) {
                System.out.println("Connected to the database test1");
            }
        } catch (ClassNotFoundException e) {
            System.err.format("Driver error");
            e.printStackTrace();
        } catch (SQLException e) {
            System.err.format("Connection error");
        }

        createTables();
    }

    public void insertSongsToDB(List<Song> listOfSongs) throws SQLException {
        for (Song song : listOfSongs) {
            String queryForPlayer = "INSERT INTO player.song (title, artist, genre, path) VALUES ('" + song.getName() + "','" + song.getArtist() + "','" + song.getGenre() + "','" + song.getPathOfTheSong() + "');";
            statement.executeUpdate(queryForPlayer);
        }
    }

    public List<Song> getSongsByGenreAndArtist(String genre, String artist) throws SQLException {
        String query = "SELECT distinct title, artist, path FROM song WHERE genre='" + genre + "' AND artist='" + artist + "';";
        List<Song> listOfSongs = new ArrayList<Song>();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            String title = resultSet.getString(1);
            String artists = resultSet.getString(2);
            String path = resultSet.getString(3);
            listOfSongs.add(new Song(path, title, artists, genre));
        }
        return listOfSongs;
    }

    public String getGenres() throws SQLException {
        String query = "SELECT distinct genre FROM song;";
        StringBuilder stringBuilder = new StringBuilder();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            stringBuilder.append(resultSet.getString(1));
            stringBuilder.append(",");
        }
        return stringBuilder.toString();
    }

    public String getArtists(String genre) throws SQLException {
        String query = "SELECT distinct artist FROM song where genre='" + genre + "';";
        StringBuilder stringBuilder = new StringBuilder();

        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            stringBuilder.append(resultSet.getString(1));
            stringBuilder.append(",");
        }
        return stringBuilder.toString();
    }

    private void createTables() {
        try {
            statement = connection.createStatement();
            String createPersonsTable = "CREATE TABLE IF NOT EXISTS song("
                    + "title   VARCHAR (100)     NOT NULL,"
                    + "artist VARCHAR (100)     NOT NULL,"
                    + "genre VARCHAR(100) NOT NULL,"
                    + "path VARCHAR (100) NOT NULL);";
            statement.executeUpdate(createPersonsTable);

            System.out.println("Tables successfully created");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void clearTable() {
        try {
            statement = connection.createStatement();
            String clearSongTableQuery = "TRUNCATE TABLE song";
            statement.executeUpdate(clearSongTableQuery);

            System.out.println("Tables successfully created");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
