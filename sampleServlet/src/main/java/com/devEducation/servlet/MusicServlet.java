package com.devEducation.servlet;

import com.devEducation.model.Song;
import com.devEducation.repository.SQLRepository;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/writeFromPathToDB")
public class MusicServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");

        String path = request.getParameter("path");
        SQLRepository sqlRepository = new SQLRepository();
        sqlRepository.clearTable();
        try {
            sqlRepository.insertSongsToDB(getDataAboutSong(path));
            out.print(sqlRepository.getGenres());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }

    public List<Song> getDataAboutSong(String pathName) {
        File folder = new File(pathName);
        File[] listOfFiles = folder.listFiles((dir, name) -> name.toLowerCase().endsWith(".mp3"));
        ArrayList<Song> songs = new ArrayList<>();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                try {
                    String inputFile = pathName + "//" + listOfFiles[i].getName();
                    String absolutePathSong = listOfFiles[i].getAbsolutePath().replace("\\", "/");
                    InputStream input = new FileInputStream(inputFile);
                    ContentHandler handler = new DefaultHandler();
                    Metadata metadata = new Metadata();
                    Parser parser = new Mp3Parser();
                    ParseContext parseCtx = new ParseContext();
                    parser.parse(input, handler, metadata, parseCtx);
                    input.close();

// Retrieve the necessary info from metadata
// Names - title, xmpDM:artist etc. - mentioned below may differ based
// on the standard used for processing and storing standardized and/or
// proprietary information relating to the contents of a file.
                    songs.add(new Song(absolutePathSong, metadata.get("title"), metadata.get("xmpDM:artist"), metadata.get("xmpDM:genre")));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                } catch (TikaException e) {
                    e.printStackTrace();
                }
            }
        }
        return songs;
    }
}
