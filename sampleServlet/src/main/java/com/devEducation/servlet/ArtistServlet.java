package com.devEducation.servlet;

import com.devEducation.repository.SQLRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/getArtistByGenre")
public class ArtistServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setCharacterEncoding("UTF-8");

        String genre = request.getParameter("genre");
        SQLRepository sqlRepository = new SQLRepository();
        try {
            out.print(sqlRepository.getArtists(genre));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}
