package com.devEducation.servlet;

import com.devEducation.model.Song;
import com.devEducation.repository.SQLRepository;
import utils.JSONSender;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = "/getSongByGenreAndArtist")
public class SongGenreServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String genre = request.getParameter("genre");
        String artist = request.getParameter("artist");
        SQLRepository sqlRepository = new SQLRepository();
        try {
            List<Song> list = sqlRepository.getSongsByGenreAndArtist(genre, artist);
            out.print(JSONSender.sendJSONResponse(list));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}
