package com.devEducation.model;

public class Song {
    String name;
    String artist;
    String genre;
    String pathOfTheSong;

    public Song(String pathOfTheSong, String name, String artist, String genre) {
        this.pathOfTheSong = pathOfTheSong;
        this.name = name;
        this.artist = artist;
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getGenre() {
        return genre;
    }

    public String getPathOfTheSong() {
        return pathOfTheSong;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}
