package sample;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import model.Song;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Controller {
    @FXML
    private TableView<Song> table;

    @FXML
    private TableColumn<Song, String> name;

    @FXML
    private TableColumn<Song, String> artist;

    @FXML
    private TableColumn<Song, String> genre;

    @FXML
    private ComboBox<String> genreComboBox;

    @FXML
    private ComboBox<String> artistComboBox;

    @FXML
    private Button playButton;

    MediaPlayer mediaPlayer;
    @FXML
    void handleOpenBtn(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open");
        File file = directoryChooser.showDialog(null);
        file.getAbsolutePath();
        String query = ("http://localhost:8080/servlet/writeFromPathToDB?path=" + file.getAbsolutePath()).replaceAll("\\\\", "/");
        HttpURLConnection connection = null;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            connection = (HttpURLConnection) new URL(query).openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    stringBuilder.append(line);
                }
                System.out.println(stringBuilder.toString());
            } else {
                System.out.println("Fail!");
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        String[] genres = stringBuilder.toString().split(",");
        ArrayList<String> listOfGenres = new ArrayList<String>();
        for (String s : genres) {
            listOfGenres.add(s);
        }
        ObservableList<String> langs = FXCollections.observableArrayList(listOfGenres);
        genreComboBox.setItems(langs);
    }

    @FXML
    void handleGenreComboBox(ActionEvent event) {
        String query = "http://localhost:8080/servlet/getArtistByGenre?genre=" + genreComboBox.getValue();
        HttpURLConnection connection = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            connection = (HttpURLConnection) new URL(query).openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    stringBuilder.append(line);
                }
                System.out.println(stringBuilder.toString());
                String[] artists = stringBuilder.toString().split(",");
                ArrayList<String> listOfArtists = new ArrayList<String>();
                for (String s : artists) {
                    listOfArtists.add(s);
                }
                ObservableList<String> langs = FXCollections.observableArrayList(listOfArtists);
                artistComboBox.setItems(langs);
            } else {
                System.out.println("Fail!");
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }


    @FXML
    void handlePlayBtn(ActionEvent event) {
        Song songToPlay = table.getSelectionModel().getSelectedItem();
        Media media = new Media(new File(songToPlay.getPathOfTheSong()).toURI().toString());
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.play();
        playButton.setText("||");
    }

    @FXML
    void handleStopBtn(ActionEvent event) {
        mediaPlayer.pause();
    }

    @FXML
    void handleReadBtn(ActionEvent event) {
        String query = "http://localhost:8080/servlet/getSongByGenreAndArtist?genre=" + genreComboBox.getValue() + "&artist=" + artistComboBox.getValue();
        String encodedUrl=query.replaceAll(" ","%20");
        HttpURLConnection connection = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            connection = (HttpURLConnection) new URL(encodedUrl).openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    stringBuilder.append(line);
                }
                String fullListSongs = stringBuilder.toString();
                System.out.println(fullListSongs);
                ObjectMapper mapper = new ObjectMapper();
                List<Song> listOfSongs = Arrays.asList(mapper.readValue(fullListSongs, Song[].class));
                prepareTable();
                table.setItems(FXCollections.observableArrayList(listOfSongs));
            } else {
                System.out.println("Fail!");
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private void prepareTable() {
        name.setCellValueFactory(new PropertyValueFactory<Song, String>("name"));
        artist.setCellValueFactory(new PropertyValueFactory<Song, String>("artist"));
        genre.setCellValueFactory(new PropertyValueFactory<Song, String>("genre"));
    }
}
