package model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize
public class Song {
    String name;
    String artist;
    String genre;
    String pathOfTheSong;

    public Song() {
        super();
    }

    public Song(String pathOfTheSong, String name, String artist, String genre) {
        this.pathOfTheSong = pathOfTheSong;
        this.name = name;
        this.artist = artist;
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getGenre() {
        return genre;
    }

    public String getPathOfTheSong() {
        return pathOfTheSong;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setPathOfTheSong(String pathOfTheSong) {
        this.pathOfTheSong = pathOfTheSong;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}